public class ContentVersionTriggerHandler {
/*
* Method Name: afterInsert
* Description: TriggerHandler method fires on after insert on Content Version Object
* @return: void
* @param: listOfContentV
*/
    public static void afterInsert(List<ContentVersion> listOfContentV) {
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        
        for (ContentVersion contVersion : listOfContentV) {
            
            if(contVersion.Dependent_Id_fileupload__c != null) {
                ContentDocumentLink objCDL = new ContentDocumentLink();
                objCDL.ContentDocumentId = contVersion.ContentDocumentId;
                objCDL.LinkedEntityId = contVersion.Dependent_Id_fileupload__c;  
                objCDL.ShareType = 'V';  
                cdlList.add(objCDL);  
            }
            
        }
        if(!cdlList.isEmpty()) {
            insert cdlList;
        }
    }
}
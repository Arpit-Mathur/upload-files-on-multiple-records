# README #


### What is this repository for? ###
This repository can be used in the scenario when we want to insert multiple files on more than one record at once. This can be done simply by using 
file-field-name & file-field-value attributes of lightning file Upload.

### How do I get set up? ###

To do this we need to have record Ids one will go in the record-id attribute which is the paremnt record on which file is uploaded.
Now to link the same files on another record we will use another set of attributes. 
file-field-name is used to store the name of the field where this second record Id will be stored and file-field-value stores the second record Id
Make sure file field name must end with _fileupload__c . A new content version record is created with this new field containing the second record Id.
 
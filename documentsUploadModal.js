import { LightningElement,api, track } from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import fetchDocuments from "@salesforce/apex/DemoController.getDocumentsForDependent";//Needs to be implemented according to use
import deleteFile from '@salesforce/apex/DemoController.deleteFile';//Needs to be implemented according to use

export default class DocumentsUploadModal extends LightningElement {
    @api firstRecordId;
    @api disabled = false;
    @api recordIds = '';
    @track spinner;
    @track documents;
    @track fileIds=[];
    @track secondRecordId = '';

    get acceptedFormats() {
        return ['.pdf', '.png','.jpg','.jpeg','.doc','.docx','.xls','.xlsx','.xlsx'];
    }
    
    connectedCallback(){
        this.getAllDocuments();
        if(this.recordIds && this.recordIds.length > 1){
            this.getSecondDependentId(this.recordIds);
        }
    }

    getSecondRecordId(recordIds){
        if(recordIds && recordIds.length>1){
            let recIds = recordIds.split(";");
            this.secondRecordId = (recIds[0] == this.firstRecordId) ? recIds[1]:recIds[0];
        }
    }

    handleUploadFinished(event){
        this.fileIds=[];
        const uploadedFiles = event.detail.files;
        let uploadedFileNames = '';
        for(let i = 0; i < uploadedFiles.length; i++) {
            uploadedFileNames += uploadedFiles[i].name + ', ';
            this.fileIds.push(uploadedFiles[i].documentId);
        }
        uploadedFileNames = uploadedFileNames.substring(0, uploadedFileNames.length-2);
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success',
                message: 'File(s) uploaded Successfully: ' + uploadedFileNames,
                variant: 'success',
            }),
        );
        setTimeout(() => {
            this.focusFirstEle();
        }, 3000);
        this.getAllDocuments();
    }
    
    getAllDocuments() {
        fetchDocuments({firstRecordId:this.firstRecordId})
        .then(result => {
            //This function is used to fetch all the documents attached on the record. 
            //Implement according to the requirement
        }).catch(error=>{
        })
    }
    
    closeModal(event) {
        const myCustomEvent = new CustomEvent("closemodal", { detail: "closeingmodal" });
        this.dispatchEvent(myCustomEvent);
    }
    filePreview(event){
        let fileId = event.target.dataset.id;
        window.open('/customerportal/sfc/servlet.shepherd/document/download/' +fileId,'self');
    }
    handleDelete(event){
        deleteFile({documentId:event.target.dataset.id})
        .then(result => {
            this.getAllDocuments();
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success!',
                    message:  'File deleted Successfully!',
                    variant: 'success',
                }),
            );
        }).catch(error=>{
        })
    }

  
}